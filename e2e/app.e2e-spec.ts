import { WordMemorizerPage } from './app.po';

describe('word-memorizer App', () => {
  let page: WordMemorizerPage;

  beforeEach(() => {
    page = new WordMemorizerPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
