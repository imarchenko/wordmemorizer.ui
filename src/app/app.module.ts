// Base
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, OnInit } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { NgSwitch } from '@angular/common';
// Angular Material
import {MatButtonModule, MatCheckboxModule, MatCardModule, MatAutocompleteModule,
        MatTableModule, MatDialogModule, MatSelectModule, MatDatepickerModule,
        MatStepperModule, MatPaginatorModule, MatProgressBarModule} from '@angular/material';
// Layouts
import { RootComponent } from './layouts/root/root.component';
import { SoonComponent } from './layouts/soon/soon.component';
import { TemplateSwitcherComponent } from './layouts/template-switcher/template-switcher.component';
// Dark theme
import { DarkTemplateComponent } from './layouts/dark-template/dark-template.component';
import { DarkHeaderComponent } from './layouts/dark-template/dark-header.component';
import { DarkFooterComponent } from './layouts/dark-template/dark-footer.component';
// Blue theme
import { BlueTemplateComponent } from './layouts/blue-template/blue-template.component';
import { BlueHeaderComponent } from './layouts/blue-template/blue-header.component';
import { BlueFooterComponent } from './layouts/blue-template/blue-footer.component';
// Components
import { HomeComponent } from './components/home/home.component';
// Pages
import { LandingComponent } from './pages/landing/landing.component';
import { VocabularyComponent } from './pages/vocabulary/vocabulary.component';

const appRoutes: Routes = [
  { path: '', component: TemplateSwitcherComponent },
  { path: 'vocabulary', component: VocabularyComponent, data: {title: 'Vocabulary'}}
  // { path: '',
  //   redirectTo: '/soon',
  //   pathMatch: 'full'
  // }
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    RootComponent,
    SoonComponent,
    TemplateSwitcherComponent,
    // Dark template
    DarkTemplateComponent,
    DarkHeaderComponent,
    DarkFooterComponent,
    // Blue template
    BlueTemplateComponent,
    BlueHeaderComponent,
    BlueFooterComponent,
    // Pages
    VocabularyComponent,
    LandingComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule, MatCheckboxModule, MatCardModule, MatAutocompleteModule,
    MatTableModule, MatDialogModule, MatSelectModule, MatDatepickerModule,
    MatStepperModule, MatPaginatorModule, MatProgressBarModule
  ],
  providers: [CookieService],
  bootstrap: [RootComponent]
})
export class AppModule { }
