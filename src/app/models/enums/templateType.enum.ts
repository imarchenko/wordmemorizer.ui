export enum TemplateType {
    Soon = 0,
    Authorization = 1,
    Landing = 2,
    BlueTemplate = 3,
    DarkTemplate = 4
}
