import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vocabulary',
  templateUrl: './views/vocabulary.component.html',
  styleUrls: ['./styles/vocabulary.component.scss']
})
export class VocabularyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
