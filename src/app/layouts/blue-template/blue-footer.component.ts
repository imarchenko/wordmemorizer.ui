import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blue-footer',
  templateUrl: './views/blue-footer.component.html',
  styleUrls: ['./styles/blue-footer.component.scss']
})
export class BlueFooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  getCurrentYear() {
    return new Date().getFullYear();
  }
}
