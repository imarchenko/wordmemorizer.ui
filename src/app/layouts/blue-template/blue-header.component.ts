import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blue-header',
  templateUrl: './views/blue-header.component.html',
  styleUrls: ['./styles/blue-header.component.scss']
})
export class BlueHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
