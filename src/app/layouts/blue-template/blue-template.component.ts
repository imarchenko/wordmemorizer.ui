import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-blue-template',
  templateUrl: './views/blue-template.component.html',
  styleUrls: ['./styles/blue-template.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BlueTemplateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
