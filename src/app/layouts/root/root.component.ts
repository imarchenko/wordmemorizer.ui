import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './views/root.component.html',
  styleUrls: ['./styles/root.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RootComponent implements OnInit {
  title = 'app';

  constructor() {
  }

  ngOnInit() {
  }

}
