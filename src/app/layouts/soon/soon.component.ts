import { Component, OnInit } from '@angular/core';
import { CookieService } from 'angular2-cookie/services/cookies.service';

@Component({
  selector: 'app-soon',
  templateUrl: './views/soon.component.html',
  styleUrls: ['./styles/soon.component.scss']
})
export class SoonComponent implements OnInit {

  constructor(private cookies: CookieService) { }

  ngOnInit() {
  }

  becameATester() {
    this.cookies.put('betaTester', 'tester');
    window.location.reload(true);
  }
}
