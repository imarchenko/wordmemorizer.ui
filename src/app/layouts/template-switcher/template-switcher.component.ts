import { Component, OnInit } from '@angular/core';
import { TemplateType } from '../../models/enums/templateType.enum';
import { CookieService } from 'angular2-cookie/services/cookies.service';

@Component({
  selector: 'app-template-switcher',
  templateUrl: './views/template-switcher.component.html',
  styleUrls: ['./styles/template-switcher.component.scss']
})
export class TemplateSwitcherComponent implements OnInit {
  template: TemplateType;
  public TemplateTypes = TemplateType;

  constructor(private cookies: CookieService) {
    const userCookie = cookies.get('user');
    if (userCookie) {
      this.template = TemplateType.BlueTemplate;
    } else {
      const betaTesterCookie = cookies.get('betaTester');
      if (betaTesterCookie) {
        this.template = TemplateType.Landing;
      } else {
        this.template = TemplateType.Soon;
      }
    }
  }

  ngOnInit() {
  }

}
