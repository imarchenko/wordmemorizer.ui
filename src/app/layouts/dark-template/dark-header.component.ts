import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dark-header',
  templateUrl: './views/dark-header.component.html',
  styleUrls: ['./styles/dark-header.component.scss']
})
export class DarkHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
