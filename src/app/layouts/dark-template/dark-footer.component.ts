import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dark-footer',
  templateUrl: './views/dark-footer.component.html',
  styleUrls: ['./styles/dark-footer.component.scss']
})
export class DarkFooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
