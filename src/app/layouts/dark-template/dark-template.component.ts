import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-dark-template',
  templateUrl: './views/dark-template.component.html',
  styleUrls: ['./styles/dark-template.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DarkTemplateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
